/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(1.5);
        System.out.println(circle);
        System.out.println("---------");
        Circle circle1 = new Circle(4.5);
        System.out.println(circle1);
        System.out.println("---------");
        Circle circle2 = new Circle(5.2);
        System.out.println(circle2);
        System.out.println("---------");
        Rectangle rectangle = new Rectangle(3,2);
        System.out.println(rectangle);
        System.out.println("---------");
        Rectangle rectangle1 = new Rectangle(4,3);
        System.out.println(rectangle1);
        System.out.println("---------");
        Sqaure sqaure =new Sqaure(4);
        System.out.println(sqaure);
        System.out.println("---------");
        Sqaure sqaure1 =new Sqaure(2);
        System.out.println(sqaure1);
        System.out.println("---------");
        
        Shape[] shapes ={ circle, circle1, circle2, rectangle, rectangle1, sqaure, sqaure1};
        for(int i=0 ; i < shapes.length; i++ ){
            System.out.println(shapes[i].getName()+ "  area :"+shapes[i].calArea());
        }
    }
}
